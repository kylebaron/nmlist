# ETABAR:        -3.3224E-02 -4.9132E-03  1.8504E-03
# SE:             2.1995E-02  2.8048E-02  3.4401E-02
#
# P VAL.:         1.3092E-01  8.6095E-01  9.5710E-01
#
# ETASHRINKSD(%)  3.2248E+01  3.9500E+00  5.3457E-01
# ETASHRINKVR(%)  5.4097E+01  7.7440E+00  1.0663E+00
# EBVSHRINKSD(%)  3.2421E+01  4.0415E+00  8.2999E-01
# EBVSHRINKVR(%)  5.4331E+01  7.9197E+00  1.6531E+00
# EPSSHRINKSD(%)  5.6421E+00
# EPSSHRINKVR(%)  1.0966E+01

handle_shk <- function(path, root = nmroot(path), nest, index = 1) {
  shk <- file.path(path, paste0(root, ".shk"))
  if(!file.exists(shk)) {
    shk <- NA
    return(shk)
  }
  if(nest > 1) {
    ans <- read_multi_tab(shk, index = index)
  } else {
    ans <- fread(shk)
  }
  ans <- as.data.frame(ans)
  ans <- ans[ans$TYPE %in% c(1,3,4,5),]
  etas <- grep("ETA", names(ans))
  neta <- length(etas)
  olbl <- paste0("OMEGA(",seq(neta), ",", seq(neta), ")")
  slbl <- paste0("SIGMA(", seq(neta), ",", seq(neta), ")")
  upop <- unique(ans$SUBPOP)
  data <- lapply(upop, function(pop) {
    ans <- ans[ans$SUBPOP==pop,]
    data1 <- data.frame(
      name = olbl,
      subpop = pop,
      etabar = unlist(ans[1, etas]),
      pval = unlist(ans[2, etas]),
      eta  = unlist(ans[3, etas]),
      stringsAsFactors = FALSE
    )
    data2 <- data.frame(
      name = slbl,
      subpop = pop,
      eps  = unlist(ans[4, etas]),
      stringsAsFactors = FALSE
    )
    data2 <- data2[data2$eps != 0,]
    rbindlist(list(data1, data2), fill = TRUE)
  })
  data <- rbindlist(data, fill = TRUE)
  rownames(data) <- NULL
  data
}
