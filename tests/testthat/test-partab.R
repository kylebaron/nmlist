library(testthat)
library(nmlist)

context("test-partab")

test_that("generate partab", {
  x <- nmlist(nmlist:::run(200))
  ans <- partab(x)
  expect_is(ans, "data.frame")
})
