% Generated by roxygen2: do not edit by hand
% Please edit documentation in R/nmlist.R
\name{heuristics}
\alias{heuristics}
\title{Print a summary of run heuristics}
\usage{
heuristics(x)
}
\arguments{
\item{x}{an \code{nmlist} object}
}
\value{
\code{NULL} (invisibly)
}
\description{
Print a summary of run heuristics
}
