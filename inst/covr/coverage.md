# coverage: 72.43%

|file             | coverage|
|:----------------|--------:|
|R/class-quiet.R  |    33.33|
|R/handle-cov.R   |    48.00|
|R/class-nmlist.R |    50.75|
|R/handle-ctl.R   |    51.25|
|R/utils.R        |    64.29|
|R/handle-grd.R   |    66.67|
|R/nmlist.R       |    74.16|
|R/handle-lst.R   |    83.54|
|R/multi-table.R  |    85.71|
|R/handle-ext.R   |    90.70|
|R/parse-ctl.R    |    92.86|
|R/handle-shk.R   |    94.44|
|R/nmlog.R        |    94.44|
|R/partab.R       |    95.45|
