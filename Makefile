SHELL := /bin/bash
PACKAGE=nmlist
VERSION=$(shell grep Version DESCRIPTION |awk '{print $$2}')
TARBALL=${PACKAGE}_${VERSION}.tar.gz
PKGDIR=.
CHKDIR=.
NMR=../nmruns/model
TARG=inst/nmruns/model
readme:
	Rscript -e 'library(rmarkdown)' -e 'render("README.Rmd")'


ec:
	echo ${VERSION}

all:
	make doc
	make build
	make install

test:
	make install
	Rscript -e 'testthat:::test_dir("tests")'

.PHONY: doc
doc:
	Rscript -e 'library(devtools); document()'

build:
	R CMD build --md5 $(PKGDIR)


install:
	R CMD INSTALL --install-tests ${TARBALL}

install-build:
	R CMD INSTALL --build --install-tests ${TARBALL}

check:
	make doc
	make build
	R CMD CHECK ${TARBALL} -o ${CHKDIR}

checkk:
	make doc
	make build
	R CMD CHECK ${TARBALL} -o ${CHKDIR} --no-examples


cprun:
	cp -r $(NMR)/$(RUN) $(TARG)

nmruns:
	make cprun RUN=200
	make cprun RUN=201
	make cprun RUN=202
	make cprun RUN=100
	make cprun RUN=106

clean-model:
	Rscript inst/script/clean-model.R

covr:
	Rscript inst/covr/covr.R
