
## MAIN IDEA

With one simple function call, get results from a nonmem run as basic
`R` objects, ready to use.

**Single overall guiding principle**: the science is at the center; the
software takes whatever shape is needed for us to do great, efficient
science. Ideally, the software blends in and isn’t noticed. We aren’t
creating a platform or ecosystem or ethos that makes science bend to the
software but it’s the other way around.

How do we get this done? We focus on

-   Simplicity
-   Efficiency

This is the only nmlist “way”.

## Distinctives

-   All R code; no binary / setup required (besides package install)
-   Will read results regardless of how the run was launched (e.g. `PsN`
    or `NONR` or `bbr`)
-   Will only return results from a single estimation method; never have
    to index results after the initial call
-   Smart processing; parses the `.ctl` code and understands what was
    requested
-   Expose low-level functions for creating your own workflows
-   Doesn’t panic if the run isn’t completed
-   Performance

### Style

-   Concise names
-   Every output is at the top level of a single list; you never have to
    think about where to look for a given item
-   More complicated outputs available from subsequent calls

## Example

``` r
library(magrittr)
library(nmlist)

project <- file.path("..", "nmruns", "model", "200")

x <- nmlist(project, deep = TRUE)
```

This is a plain list:

``` r
class(x)
```

    . [1] "nmlist" "list"

What’s in there?

``` r
x
```

    .      root:  200 
    .     index:  2 
    .       ext:  200.ext 
    .     theta:  <theta values (list)> 
    .     omega:  <omega values (matrix)> 
    .     sigma:  <sigma values (matrix)> 
    .    partab:  <parameter table (data.frame)> 
    .   version:  7.4.4 
    .    method:  IMP 
    .  nrec/nid:  4292/160 
    .      npar:  9 
    .       ofv:  31297.97 
    . condition:  8 
    .      corr:  -0.45 
    .    hreset:  FALSE 
    .   minterm:  FALSE 
    .      term:  TRUE 
    .  boundary:  FALSE 
    .      done:  TRUE

``` r
names(x)
```

    .  [1] "root"      "path"      "version"   "index"     "ext"       "theta"    
    .  [7] "omega"     "sigma"     "partab"    "shk"       "method"    "meth"     
    . [13] "nest"      "nrec"      "nid"       "nobs"      "npar"      "ofv"      
    . [19] "corr"      "condition" "hreset"    "bound"     "minterm"   "term"     
    . [25] "termm"     "tere"      "done"      "env"

## Basics

We have “everything” in this list. You access results at the top level
by a simple name:

``` r
x$done
```

    . [1] TRUE

``` r
x$ofv
```

    . [1] 31297.97

``` r
x$condition
```

    . [1] 8.44973

``` r
x$corr
```

    . [1] -0.447867 -0.434073 -0.399916  0.396523  0.344612

``` r
x$method
```

    . [1] "IMP"

``` r
x$shk
```

    .          name subpop     etabar      pval      eta     eps
    . 1: OMEGA(1,1)      1 -0.0131734 0.5982990 33.79980      NA
    . 2: OMEGA(2,2)      1  0.0356803 0.2092870  4.77269      NA
    . 3: OMEGA(3,3)      1 -0.0685982 0.0456761  2.21708      NA
    . 4: SIGMA(1,1)      1         NA        NA       NA 6.32934

``` r
x$nrec
```

    . [1] 4292

``` r
x$nid
```

    . [1] 160

``` r
x$partab
```

    .          name estimate     value        se fixed
    . 1      THETA1    0.575 0.5753780 0.0711620     0
    . 2      THETA2      4.1 4.0963800 0.0427686     0
    . 3      THETA3     1.16 1.1649400 0.0416109     0
    . 4      THETA4     4.17 4.1707100 0.0189102     0
    . 5      THETA5     1.19 1.1852400 0.0354671     0
    . 6  SIGMA(1,1)   0.0411 0.0411135 0.0010926     0
    . 7  OMEGA(1,1)     0.23 0.2296950 0.0588286     0
    . 8  OMEGA(2,1)        0 0.0000000 0.0000000     1
    . 9  OMEGA(2,2)    0.143 0.1433920 0.0221338     0
    . 10 OMEGA(3,1)        0 0.0000000 0.0000000     1
    . 11 OMEGA(3,2)        0 0.0000000 0.0000000     1
    . 12 OMEGA(3,3)    0.198 0.1984210 0.0235273     0

Only one method is returned

``` r
nmlist(project, index = 1)
```

    .      root:  200 
    .     index:  1 
    .       ext:  200.ext 
    .     theta:  <theta values (list)> 
    .     omega:  <omega values (matrix)> 
    .     sigma:  <sigma values (matrix)> 
    .    partab:  <parameter table (data.frame)> 
    .   version:  7.4.4 
    .    method:  SAEM 
    .  nrec/nid:  4292/160 
    .      npar:  9 
    .       ofv:  29564.97 
    . condition:  8 
    .      corr:  -0.41 
    .    hreset:  FALSE 
    .   minterm:  FALSE 
    .      term:  TRUE 
    .  boundary:  FALSE 
    .      done:  TRUE

We do have other funtions that help create more sophisticated outputs

``` r
partab(x)
```

    .     root       name     value        se      shk  diag fixed
    .  1:  200     THETA1 0.5753780 0.0711620       NA FALSE     0
    .  2:  200     THETA2 4.0963800 0.0427686       NA FALSE     0
    .  3:  200     THETA3 1.1649400 0.0416109       NA FALSE     0
    .  4:  200     THETA4 4.1707100 0.0189102       NA FALSE     0
    .  5:  200     THETA5 1.1852400 0.0354671       NA FALSE     0
    .  6:  200 SIGMA(1,1) 0.0411135 0.0010926  6.32934  TRUE     0
    .  7:  200 OMEGA(1,1) 0.2296950 0.0588286 33.79980  TRUE     0
    .  8:  200 OMEGA(2,1) 0.0000000 0.0000000       NA FALSE     1
    .  9:  200 OMEGA(2,2) 0.1433920 0.0221338  4.77269  TRUE     0
    . 10:  200 OMEGA(3,1) 0.0000000 0.0000000       NA FALSE     1
    . 11:  200 OMEGA(3,2) 0.0000000 0.0000000       NA FALSE     1
    . 12:  200 OMEGA(3,3) 0.1984210 0.0235273  2.21708  TRUE     0

## Log

``` r
nmlog("100, 106, 200", project = "../nmruns/model/")
```

    .   root      ofv delta condition npar
    . 2  106 30904.41   0.0      48.5   15
    . 1  100 31296.55 392.1       7.3    9
    . 3  200 31297.97 393.6       8.4    9

## Run diagnostics

``` r
diagnostics_log("../nmruns/model")
```

    .   root hreset term tere minterm bound round                              termm
    . 1  100   TRUE TRUE TRUE   FALSE FALSE FALSE           0MINIMIZATION SUCCESSFUL
    . 2  106  FALSE TRUE TRUE   FALSE FALSE FALSE           0MINIMIZATION SUCCESSFUL
    . 3  200  FALSE TRUE TRUE   FALSE FALSE FALSE EXPECTATION ONLY PROCESS COMPLETED
    . 4  201  FALSE TRUE TRUE   FALSE FALSE FALSE EXPECTATION ONLY PROCESS COMPLETED
    . 5  202  FALSE TRUE TRUE   FALSE FALSE FALSE EXPECTATION ONLY PROCESS COMPLETED

## Get anything

``` r
x <- nmlist("../nmruns/model/100")

root.grd(x) %>% head()
```

    .   ITERATION   name    value
    . 1         0 THETA1 1608.070
    . 2         1 THETA1 2623.670
    . 3         2 THETA1 1048.440
    . 4         3 THETA1 -441.812
    . 5         4 THETA1 -836.455
    . 6         5 THETA1 -874.922

``` r
root.phi("../nmruns/model/200") %>% head()
```

    .   SUBJECT_NO ID    PHI(1)     PHI(2)     PHI(3)  PHC(1,1)   PHC(2,1)   PHC(2,2)
    . 1          1  1 -0.104490 -0.4057740 -0.2734570 0.0907784 0.01215570 0.00969057
    . 2          2  2  0.429406 -0.4960100 -0.6483280 0.0904203 0.01321860 0.01086020
    . 3          3  3 -0.479693 -0.2554340  0.0357443 0.0805306 0.01860150 0.01274120
    . 4          4  4  0.659649  0.7733720  0.7262850 0.0986206 0.00950889 0.00899805
    . 5          5  5  0.544525  0.8198210  0.6043380 0.1209620 0.01383210 0.01006220
    . 6          6  6 -0.152126  0.0332761  0.1280440 0.0764405 0.01266540 0.00888332
    .      PHC(3,1)     PHC(3,2)   PHC(3,3)      OBJ
    . 1 -0.00531275 -0.000951231 0.00909489 80.21026
    . 2 -0.00324822 -0.000800013 0.01079490 94.46432
    . 3 -0.00555500 -0.002240540 0.01407730 72.11683
    . 4 -0.00682676 -0.005201640 0.03026500 62.51592
    . 5 -0.01263610 -0.009284440 0.07342300 58.06979
    . 6 -0.00203412 -0.000385686 0.00949885 81.67894

## Performance

``` r
library(microbenchmark)
microbenchmark(nmlist(project))
```

    . Unit: milliseconds
    .             expr      min       lq     mean   median       uq      max neval
    .  nmlist(project) 5.211429 5.370494 5.845933 5.498899 6.141834 8.075634   100
